# spotify

## Project setup
Make sure to configure API URL in config>services.json

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Vuetify documentation
See [Vuetify](https://vuetifyjs.com/en/).
