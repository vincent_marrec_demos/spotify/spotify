import Vue from "vue";
import VueRouter from "vue-router";

import store from "@/store/index.js";

import Login from "@/views/Login.vue";
import Search from "@/views/Search.vue";
import Library from "@/views/Library.vue";

Vue.use(VueRouter);

const routes = [
	{
		path: "/",
		name: "Library",
		component: Library,
	},
	{
		path: "/login",
		name: "Login",
		component: Login,
	},
	{
		path: "/search",
		name: "Search",
		component: Search,
	},
];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes,
});

router.beforeEach((to, from, next) => {
	if (to.name !== "Login" && !store.state.user) {
		next("/login");
	} else {
		next();
	}
});

export default router;
