import axios from "axios";

import services from "../../config/services.json";

const API_URL = services.api;

export default {
	async getUsers() {
		return (await axios.get(`${API_URL}/users`)).data;
	},
	async getAlbums(user) {
		return (await axios.get(`${API_URL}/users/${user.id}/albums`)).data;
	},
	async addAlbum(user, album) {
		return (await axios.post(`${API_URL}/users/${user.id}/albums`, album))
			.data;
	},
	async removeAlbum(user, album) {
		return (
			await axios.delete(`${API_URL}/users/${user.id}/albums/${album.id}`)
		).data;
	},
	async favoriteAlbum(user, album) {
		return (
			await axios.patch(
				`${API_URL}/users/${user.id}/albums/${album.id}`,
				{ favorite: !album.favorite }
			)
		).data;
	},
	async addTag(user, tag, albums) {
		return (
			await axios.patch(`${API_URL}/users/${user.id}/albums`, {
				tag,
				ids: albums.map((a) => a.id),
			})
		).data;
	},
	async searchAlbums(query) {
		return (await axios.get(`${API_URL}/search?q=${query}`)).data;
	},
};
